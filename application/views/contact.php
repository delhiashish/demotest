<?php   $this->load->view('common/header');  ?>
<div class="breadcrumbs breadcrumbs-sections">
	<ul>
		<li class="breadcrumbs-home">
			<a href="#" title="Back To Home">
				<i class="fa fa-home"></i>
			</a>
		</li>
		<li>
			Contact Us
		</li>
	</ul>
</div>
<div class="content_info content_resalt-img">
	<div class="bg-content">
		<div class="title-vertical-line">
			<h2><span>Feel Free To Contact Us</span></h2>
			<p class="lead-2">Send your query to us. Our team ll connect you shortly.</p>
			<p class="pt-5 dark-gray">We are here ready to solve your problem. We ll connect you soon to work upon your query. <br> CodeAspire is a fast growing it industry, providing a non stopable solution to our clients</p>
		</div>
		<div class="padding-top padding-bottom-mini">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<?php echo  get_flashdata('message'); ?>
						<div class="container-form">
							<form class="form-theme form-contact-us" action="#" method="POST" id="contactform" >
								<input type="text" placeholder="Name" name="name" required="">
								<input type="email" placeholder="Email*" name="email" required="">
								<input type="number" placeholder="Phone*" name="number" required="">
								<textarea placeholder="Your Message" name="message" required=""></textarea>
								<input type="submit" name="submit" value="Send Message" class="btn btn-primary">
							</form> 
						</div>
						<div id="result"></div>  
					</div>
					<div class="col-md-4">
						<div class="contact-list-container">
							<ul class="contact-list">
								<li>
									<h4><i class="fa fa-envelope-o"></i>Email:</h4>
									<a href="#">demo@mail.com</a>
								</li>
								<li>
									<h4><i class="fa fa-fax"></i>Phones:</h4>
									<p>+91-9876543210</p>
								</li>

								<li>
									<h4><i class="fa fa-clock-o"></i>Working hours:</h4>
									<p>Open 10 AM - 6 PM on Saturday to Monday</p>
								</li>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php   $this->load->view('common/footer');  ?>



