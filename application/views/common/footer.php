
</div>

<!-- jQuery local--> 
<script src="<?php echo base_url(); ?>/assets/frontend/js/libs/jquery.js"></script>  
  
<!--Totop-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/js/totop/jquery.ui.totop.js" ></script>   
<!--Slide Revolution-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/js/rs-plugin/js/jquery.themepunch.tools.min.js" ></script>      
<script type='text/javascript' src='<?php echo base_url(); ?>/assets/frontend/js/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>    
<!-- Maps -->
<script src="<?php echo base_url(); ?>/assets/frontend/js/maps/gmap3.js"></script>            
<!--Ligbox--> 
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/js/fancybox/jquery.fancybox.js"></script> 
<!-- owl.carousel.min.js-->
<script src="<?php echo base_url(); ?>/assets/frontend/js/carousel/owl.carousel.min.js"></script>
<!-- Filter -->
<script src="<?php echo base_url(); ?>/assets/frontend/js/filters/jquery.isotope.js" type="text/javascript"></script>
<!-- Parallax-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/js/parallax/parallax.min.js"></script>  
<!--Theme Options-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/js/theme-options/theme-options.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/js/theme-options/jquery.cookies.js"></script> 
<!-- Bootstrap.js-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/js/bootstrap/bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/js/bootstrap/bootstrap-slider.js"></script> 
<!--MAIN FUNCTIONS-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/js/main.js"></script>


</body>
</html>