<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?php echo $page_title; ?></title>
  <meta name="keywords" content="" />
  <meta name="description" content="">
  <meta name="author" content="">  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="<?php echo base_url(); ?>/assets/frontend/css/style.css" rel="stylesheet" media="screen">
  <link href="<?php echo base_url(); ?>/assets/frontend/css/theme-responsive.css" rel="stylesheet" media="screen">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/frontend/img/icons/fav.png">
  <link rel="apple-touch-icon" href="<?php echo base_url(); ?>/assets/frontend/img/icons/apple-touch-icon.png">
</head>
<body>

  <div class="preloader">
    <div class="status">&nbsp;</div>
  </div>
  <div id="fond-header" class="fond-header"></div>
  <div id="layout" class="layout-wide pt-70">
    <div class="content-central">



