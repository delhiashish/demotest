<?php


class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
			$this->load->model('homemodel');
	}


	public function contact()
	{
		$this->data['page_title'] = "Contact us";
		$this->load->view('contact',$this->data);
	}
	
}
