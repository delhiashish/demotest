-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2018 at 04:22 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.0.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codeaspire`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(30) DEFAULT NULL,
  `admin_email` varchar(30) DEFAULT NULL,
  `admin_pass` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_email`, `admin_pass`) VALUES
(1, 'ashish verma', '123', '123'),
(2, 'adss', 'admin@gmail.com', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE `enquiry` (
  `enqid` int(11) NOT NULL,
  `ename` varchar(50) DEFAULT NULL,
  `eemail` varchar(50) DEFAULT NULL,
  `eaddress` varchar(50) DEFAULT NULL,
  `total_enquiry` int(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiry`
--

INSERT INTO `enquiry` (`enqid`, `ename`, `eemail`, `eaddress`, `total_enquiry`) VALUES
(1, 'ashish verma', 'ashish.verma@codeyeti.in', 'i wanna aplly for the job', 3),
(11, 'ashish', 'ashish@gmal.com', 'ddsf', 1),
(15, 'ashish', 'asasa@frfd.om', 'dsds', 1),
(16, 'ravi', 'ravi.mall@codeyeti.in', 'hi', 0);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(2) NOT NULL,
  `project_name` varchar(20) NOT NULL,
  `project_description` varchar(200) NOT NULL,
  `image` varchar(100) NOT NULL,
  `isverified` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `project_name`, `project_description`, `image`, `isverified`) VALUES
(2, 'nbnbn', 'ugjvvbn', '20708083_1661606723906123_8356002589027039837_n__09022018_003351__.png', 1),
(6, 'mnvudkds', 'hjbjh', '20708083_1661606723906123_8356002589027039837_n__09022018_205139__.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscribe`
--

CREATE TABLE `subscribe` (
  `subsid` int(11) NOT NULL,
  `subsemail` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe`
--

INSERT INTO `subscribe` (`subsid`, `subsemail`) VALUES
(1, 'ashishk.verma95@gmail.com'),
(2, 'ashishk.verma95@gmail.com'),
(3, 'anshulikva@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `fname` varchar(25) NOT NULL,
  `lname` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `phone` int(20) NOT NULL,
  `password` varchar(25) NOT NULL,
  `profession` varchar(25) NOT NULL,
  `image` text NOT NULL,
  `isverified` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `fname`, `lname`, `email`, `phone`, `password`, `profession`, `image`, `isverified`) VALUES
(5, 'anshulika', 'verma', 'admin@gmail.com', 2147483647, '123456', 'developer', '20708083_1661606723906123_8356002589027039837_n__08022018_022023__.png', 1),
(6, 'anshulika', 'verma', 'admin@gmail.com', 2147483647, '1234', 'developer', '20708083_1661606723906123_8356002589027039837_n__08022018_222612__.png', 0),
(8, 'mvnzvdndunjefkjd', 'vmnivdihvyhdvid', 'admin@gmail.com', 2147483647, '123456', 'xdv', '20708083_1661606723906123_8356002589027039837_n__09022018_204932__.png', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD PRIMARY KEY (`enqid`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe`
--
ALTER TABLE `subscribe`
  ADD PRIMARY KEY (`subsid`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `enquiry`
--
ALTER TABLE `enquiry`
  MODIFY `enqid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `subsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
